package com.example.firstpro

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.firstpro.fragment.FirstFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button = findViewById<Button>(R.id.bt_cool)

        button.setOnClickListener {

            val manager = supportFragmentManager

            val transaction = manager.beginTransaction()

            val firstFragment = FirstFragment()

            transaction.replace(R.id.container, firstFragment).addToBackStack(null).commit()
        }
    }
}
